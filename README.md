Training CorTexT-Risis
======================
SHORT COURSE TYPE A (SCA)

10 -12 May 2017

See : http://risis.eu/events/

Organized by CorTexT-Lab Team
of LISIS Unit in Paris-Est

Table of Contents
----------------

1. OVERVIEW OF RISIS INFRASTUCTURE ..........................................3
1. THE RISIS CORE FACILITY ..............................................................4
1. DOCUMENTATION OF THE RISIS CORTEXT PLATFORM................5
1. MAIN FEATURES OF RISIS CORTEXT .............................................6
   - CORPUS BUILDING AND EXPLORER................................................................................................7
   - TEXTUAL PRE-PROCESSING AND PROCESSING.........................................................................9
   - TIME-BASED DATA PROCESSING...................................................................................................14
   - DATA COMPUTING AND ANALYSIS...............................................................................................16
